def hello():
    print('howdy')
    print('HOWDY')
    print('ello there')


hello()
hello()
hello()

print()


def hello(name):
    print('Hello ' + name)


hello('Crash')
hello('Override')

#   None -  represents the absence of a value
spam = print('Hello')
None == spam
print()

# print('Hello', end='') -  to remove printing new line
print('Hello', end='')
print('World')
print()
#   sep=','        - removes spaces
print('cats', 'dogs', 'mice', sep=',')
print()


        #scope of variables
#If you ever want to modify the value stored in a global variable from in a function,
# you must use a global statement on that variable.
def spam():
    global eggs
    eggs = 'spam'


def bacon():
    eggs = 'bacon'


def ham():
    print(eggs)


eggs = 42
spam()
print(eggs)
print()


# exception - error handling
def spam(divideBy):
    try:
        return 42 / divideBy
    except ZeroDivisionError:
        print('Error: Invalid argument.')


print(spam(2))
print(spam(12))
print(spam(0))
print(spam(1))
print()


def spam(divideBy):
    return 42 / divideBy

try:
    print(spam(2))
    print(spam(12))
    print(spam(0))
    print(spam(1))
except ZeroDivisionError:
    print('Error: invalid arg')
print()




