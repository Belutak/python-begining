import pprint

# myCat = {'size': 'fat', 'color': 'gray', 'disposition': 'loud'}
# print(myCat['size'])

# birthdays = {'Alice': 'Mar 15', 'Zoran': 'Feb 10', 'Carol': 'Sep 14'}
#
# while True:
#     print('Enter a name: (blank to quit)')
#     name = input()
#     if name == '':
#         break
#
#     if name in birthdays:
#         print(birthdays[name] + ' is the birthday of ' + name)
#     else:
#         print('I do not have b-day infor for name ' + name)
#         print('What is their b-day?')
#         bday = input()
#         birthdays[name] = bday
#         print('Birthday database updated')


spam = {'color': 'red', 'age': 42}
for v in spam.values():
    print(v)

for v in spam.keys():
    print(v)

for v in spam.items():
    print(v)

spam = {'name': 'Zophie', 'age': 7}
print('name' in spam.keys())


picnicItems = {'apples': 5, 'cups': 2}
print('I am bringing ' + str(picnicItems.get('cups', 0)) + ' cups.')
print('I am bringing ' + str(picnicItems.get('eggs', 0)) + ' eggs.')

spam = {'name': 'Pooka', 'age': 5}
if 'color' not in spam:
    spam['color'] = 'black'

for v in spam.items():
    print(v)

print()
spam = {'name': 'Pooka', 'age': 5}
spam.setdefault('color', 'black')
for v in spam.items():
    print(v)

print()
spam.setdefault('color', 'white')
for v in spam.items():
    print(v)


print()
print()
message = 'It was a bright cold day in April, and the clocks were striking thirteen.'
count = {}

for character in message:
    count.setdefault(character, 0)
    count[character] = count[character] + 1

pprint.pprint(count)




