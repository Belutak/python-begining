spam = ['cat', 'bat', 'rat', 'elephant']
print(spam)
print(spam[0])
print()

spam = [['cat', 'bat'], [10, 20, 30, 40, 50]]
print(spam[0][1])
print(spam[1][4])
print()
# -1 refers to the last index in a list,
# the value -2 refers to the second-to-last index in a list, and so on
spam = ['cat', 'bat', 'rat', 'elephant']
print(spam[-1])
print()

#       slice
#A slice goes up to, but will not include, the value at the second index
spam = ['cat', 'bat', 'rat', 'elephant']
print(spam[0:4])
print(spam[1:3])
print(spam[0:-1])
print(spam[:2])
print(spam[1:])
print(spam[:])
print()


print(len(spam))
print()

spam[1] = 'smthing'
print(spam)
spam[2] = spam[1]
print(spam[2])
print()

#    +   to combine two lists
spam = [1, 2, 3]
spam = spam + ['A', 'B', 'C']
print(spam)
print()

#  *  can be used to multiply lists
spam = [1, 2, 3] * 3
print(spam)
print()

#   del     vill delete value at index in list
spam = ['cat', 'bat', 'rat', 'elephant']
del spam[2]
print(spam)
print()
print()
print()


'''
catNames = []
while True:
    print('Enter the name of cat ' + str(len(catNames) + 1) +
          ' (Or enter nothing to stop.):')
    name = input()
    if name == '':
        break
    catNames = catNames + [name] # list concatenation
print('The cat names are:')
for name in catNames:
    print('  ' + name)
'''



#   for loop
supplies = ['pens', 'staplers', 'flame-throwers', 'binders']
for i in range(len(supplies)):
    print('intex ' + ' in supplies is: ' + supplies[i])
print()
print()


#   in     and       not in    operators
print('howdy' in ['hello', 'hi', 'howdy', 'heyas'])

print('bat' in spam)
print('bat' not in spam)
print()

myPets = ['Zophie', 'Pooka', 'Fat-tail']
print('Enter a pet name:')
#name = input()
name = 'uncomment up, comment this'
if name not in myPets:
    print('I do not have a pet named ' + name)
else:
    print(name + ' is my pet.')
print()

#   multiple assignment
cat = ['fat', 'orange', 'loud']
size, color, disposition = cat
print(color)

a, b = 'alice', 'bob'
a, b = b, a
print(a)
print()


#A method is the same thing as a function, except it is “called on” a value.
print(spam.index('bat'))
print()


#   append()   - to add new element to list,    insert() - to insert where we want
spam.append('moose')
print(spam)
spam.insert(1, 'chicken')
print(spam)
print()

#       remove()
print(spam)
spam.remove('bat')
print(spam)
print()

#    sort()   - sorting the values in list with
junk = [2, 5, 3.14, 1, -7]
junk.sort()
print(junk)
junk = ['ants', 'cats', 'dogs', 'badgers', 'elephants']
junk.sort()
print(junk)
junk.sort(reverse=True)
print(junk)
print()
# sort() uses “ASCIIbetical order” rather than actual alphabetical order for sorting strings

#sort the values in regular alphabetical order, pass str.
# lower for the key keyword argument in the sort() method call
junk = ['a', 'z', 'A', 'Z']
junk.sort(key=str.lower)
print(junk)
print()



#   STRINGS
# string is a “list” of single text characters
# a string is immutable
name = 'lordnycon'
print(name[0])

print('rd' in name)

for i in name:
    print('---' + i + '---')

# to change string use slicing and concatenation -
# to build a new string by copying from parts of the old string
name = 'Zophie a cat'
newName = name[0:7] + 'the' + name[8:12]
print(newName)

#to modify list
eggs = [1, 2, 3]
del eggs[2]
del eggs[1]
del eggs[0]
eggs.append(4)
eggs.append(5)
eggs.append(6)
print(eggs)
print()
print()



#       TUPLES
#tuples are typed with parentheses
#immputable
eggs = ('hello', 42, 0.5)
print(eggs[0])
print(eggs[1:3])
print()
#If you have only one value in your tuple, -
# you can indicate this by placing a trailing comma after the value inside the parentheses
bacon = ('hello',)
print(type(bacon))
#If you need an ordered sequence of values that never changes, use a tuple.
print()

#   list()  and tuple()   - will return list and tuple versions of the values passed to them
egg = tuple(['cat', 'dog', 5])
print(type(egg))
bacon = list('hello')
print(bacon)
print()
print()




#       REFERENCES
#When you assign a list to a variable, you are actually assigning
# a list reference to the variable
spam = [0, 1, 2, 3, 4, 5]
cheese = spam
cheese[1] = 'Hello'
print(spam)
print()


def eggs(someParameter):
    someParameter.append('Hello')


spam = [1, 2, 3]
eggs(spam)
print(spam)
print()


#   The copy Module’s copy() and deepcopy() Functions
#if the function modifies the list or dictionary that is passed,
# you may not want these changes in the original list or dictionary value;
#copy.copy(), can be used to make a duplicate copy of a mutable value like a -
# list or dictionary, not just a copy of a reference:
import copy
spam = ['A', 'B', 'C', 'D']
cheese = copy.copy(spam)
cheese[1] = 42
print(spam)
print(cheese)
#If the list you need to copy contains lists,
# then use the copy.deepcopy() function instead of copy.copy()
print()
print()
print('=================================================')

