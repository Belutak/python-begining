print(True and False)
print(True and True)
        #The three Boolean operators (and, or, and not)
print()
print(True or True)
print(True or False)
print(False or False)
print()

# 'not' works only on booleans
print(not True)
print(not False)
print()

print((4 < 5) and (3 < 8))
print((4 > 6) or (5 > 6))
print((4 == 4) or (1 == 2))
print()

print(2 + 2 == 4 and not 2 + 2 == 5 and 2 * 2 == 2 + 2)
print()
print()

name = 'Zero'
password = 'cool'
if name == 'Zero':
    print('hello Zero')
    if password == 'cool':
        print('access granted!')
    else:
        print('wrong password!')

#In Python, an if statement consists of the following:
#The if keyword
#A condition (that is, an expression that evaluates to True or False)
#A colon
#Starting on the next line, an indented block of code (called the if clause)


#An else statement doesn’t have a condition, and in code, an else statement always consists of the
# following:
#The else keyword
#A colon
#Starting on the next line, an indented block of code (called the else clause)


#In code, an elif statement always consists of the following:
#The elif keyword
#A condition (that is, an expression that evaluates to True or False)
#A colon
#Starting on the next line, an indented block of code (called the elif clause)
print()
print()
print()

name = 'Dracula'
age = 4000
if name == 'Alice':
    print('Hi, Alice.')
elif age < 12:
    print('You are not Alice, kiddo.')
elif age > 2000:
    print('Unlike you, Alice is not an undead, immortal vampire.')
elif age > 100:
    print('You are not Alice, grannie.')



# In code, a while statement always consists of the following:
# The while keyword
# A condition (that is, an expression that evaluates to True or False)
# A colon
# Starting on the next line, an indented block of code (called the while clause)

spam = 0
while spam < 5:
    print('heyo')
    spam = spam + 1
'''
name = ''
while name != 'your name':
    print('Please type your name.')
    name = input()
print('Thank you!')
'''

'''
while True:
    print('Please type your name.')
    name = input()                  
    if name == 'your name':         
        break                       
print('Thank you!')  
'''



#continue returns to begining of loop
'''
while True:
  print('Who are you?')
  name = input()
  if name != 'Joe':       
    continue             
  print('Hello, Joe. What is the password? (It is a fish.)') 
  password = input()     
  if password == 'swordfish':
    break                 
print('Access granted.')  
'''


#When used in conditions, 0, 0.0, and '' (the empty string) are considered False,
# while all other values are considered True.
'''
name = ''
while not name:
    print('Enter your name:')
    name = input()
print('How many guests will you have?')
numOfGuests = int(input())
if numOfGuests:
    print('Be sure to have enough room for all your guests.')
print('Done')
'''



#In code, a for statement looks something like for i in range(5): and
#always includes the following:
#The for keyword
#A variable name
#The in keyword
#A call to the range() method with up to three integers passed to it
#A colon
#Starting on the next line, an indented block of code (called the for clause)
print('My name is')
for i in range(5):
    print('Jimmy Five Times (' + str(i) + ')')
print()
print()


#  range()

#Some functions can be called with multiple arguments separated by a comma,
#and range() is one of them. This lets you change the integer passed to range()
#to follow any sequence of integers, including starting at a number other than zero.
for i in range(12, 16):
    print(i)
#The range() function can also be called with three arguments.
#The first two arguments will be the start and stop values,
#and the third will be the step argument. The step is the amount
#that the variable is increased by after each iteration.
for i in range(0, 10, 2):
    print(i)
#to count negative
for i in range(5, -1, -1):
    print(i)
print()
print()


#     import
# import random, sys, os, math          //import several modules at once
import random
for i in range(5):
    print(random.randint(1, 10))
print()



#           sys.exit()
#you can cause the program to terminate, or exit, by calling the sys.exit()
# you have to import sys before your program can use it
import sys

while True:
    print('Type exit to exit.')
    response = input()
    if response == 'exit':
        sys.exit()
    print('You typed ' + response + '.')


