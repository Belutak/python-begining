y = 7
print(y)
print(y)

print(2 ** 8)
print(23 // 7)

print('Alice' + 'Bob')
# print('alice' + 3)
print('Alice' * 5)

spam = 40
print(spam)
eggs = 2
print(spam + eggs)
spam = spam + eggs
print(spam)

print('Hello world')
print('What is your name?')
my_name = input()
print('It is good to meet you, ' + my_name)
print('the length of your name is:')
print(len(my_name))
print('what is your age?')
my_age = input()
print('You will be ' + str(int(my_age) + 1) + ' in a year')
