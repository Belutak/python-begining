def collatz(number):
    if number % 2 == 0:
        return number // 2
    else:
        return 3 * number + 1


print('enter number:')
inputVal = int(input())


while True:
    returnedVal = collatz(inputVal)
    print(returnedVal)
    inputVal = returnedVal
    if returnedVal == 1:
        break
    else:
        True
