

backpack = {'arrow': 12, 'gold coin': 42, 'rope': 1, 'torch': 6, 'dagger': 1}


def displayInventory(stuff):
    total = 0
    for k, v in stuff.items():
        print(str(v) + ' ' + k)
        total += v
    print('Total number of items: ' + str(total))

displayInventory(backpack)